package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.DefaultUserApprovalHandler;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
public class OAuth2ProviderConfig extends OAuth2Configuration {

    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired
        private TokenStore tokenStore;

        //@Autowired
        private UserApprovalHandler userApprovalHandler = new DefaultUserApprovalHandler();

        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            /*
            Configure OAuth2 clients that include client_id, client_secret, grant_type, resource ids, scopes and authorities
             */
            // TODO munich airport: Replace it with JDBC version when there is a need for a dynamic client configurations
            //DataSource clientDataSource = null; // TODO define JDBC datasource for client data
            /* TODO datasource should match this statments
            CLIENT_FIELDS_FOR_UPDATE = "resource_ids, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove";
	        CLIENT_FIELDS = "client_secret, " + CLIENT_FIELDS_FOR_UPDATE;

	        BASE_FIND_STATEMENT = "select client_id, " + CLIENT_FIELDS + " from oauth_client_details";

            DEFAULT_FIND_STATEMENT = BASE_FIND_STATEMENT + " order by client_id";
            DEFAULT_SELECT_STATEMENT = BASE_FIND_STATEMENT + " where client_id = ?";
            DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ", client_id) values (?,?,?,?,?,?,?,?,?,?,?)";
            DEFAULT_UPDATE_STATEMENT = "update oauth_client_details " + "set " + CLIENT_FIELDS_FOR_UPDATE.replaceAll(", ", "=?, ") + "=? where client_id = ?";
            DEFAULT_UPDATE_SECRET_STATEMENT = "update oauth_client_details " + "set client_secret = ? where client_id = ?";
            DEFAULT_DELETE_STATEMENT = "delete from oauth_client_details where client_id = ?";
             */
            // @formatter:off
            //clients.jdbc(clientDataSource)

            clients.inMemory() // DUMMY-VERSION in-memory
                    .withClient("known-app")
                    .authorizedGrantTypes("password")
                    .authorities("public", "register", "private")
                    .scopes("public", "register", "private")
                    .secret("topsecret")
                    .resourceIds(RESOURCE_ID)
                    .and()
                    .withClient("anonym-app")
                    .authorizedGrantTypes("client_credentials")
                    .authorities("public", "register")
                    .scopes("public", "register")
                    .resourceIds(RESOURCE_ID)
                    .secret("secret");
            // @formatter:on
        }


        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.tokenStore(tokenStore).userApprovalHandler(userApprovalHandler)
                    .authenticationManager(authenticationManager);
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
            oauthServer.realm("muc/client");
        }

    }

}

package com.example.config;

import com.google.common.collect.ImmutableList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new StandardPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder passwordEncoder = passwordEncoder();
//        // TODO munich airport: replace this with a JDBC user database
//        DataSource dataSource = null; // TODO define datasource
//        String usersByUsernameQuery = "select username,password,enabled from users where username=?"; // TODO define query
//        String authoritiesByUsernameQuery = "select username, roleName from role..."; // TODO define query
//        String groupAuthoritiesByUsername = "select g.id, g.group_name, ga.authority from ..."; // TODO define query
//        auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder())
//                .usersByUsernameQuery(usersByUsernameQuery)
//                .authoritiesByUsernameQuery(authoritiesByUsernameQuery)
//                .groupAuthoritiesByUsername(groupAuthoritiesByUsername);
//     // .defaultSchema() not recommended if your database not support default schema SQL http://docs.spring.io/spring-security/site/docs/3.0.x/reference/appendix-schema.html

        // DUMMY VERSION in-memory
        auth.inMemoryAuthentication().passwordEncoder(passwordEncoder)
                .withUser("marissa").password(passwordEncoder.encode("koala")).roles("inhouse", "private", "public", "register")
            .and()
                .withUser("paul").password(passwordEncoder.encode("emu")).authorities(ImmutableList.of());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    @Bean
    public UserDetailsService userDetailsServiceBean() throws Exception {
             return super.userDetailsServiceBean();
    }

    @Bean
    public UserDetailsManager userDetailsManager(AuthenticationManager authenticationManager) throws Exception {
        // TODO Wire this jdbcUserDetailsManager together with a valid datasource
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setAuthenticationManager(authenticationManager);
//        jdbcUserDetailsManager.setDataSource(TODO);
//        return jdbcUserDetailsManager;

        // TODO remove this dummy mock
        return new UserDetailsManager() {
            @Override
            public void createUser(UserDetails user) {

            }

            @Override
            public void updateUser(UserDetails user) {

            }

            @Override
            public void deleteUser(String username) {

            }

            @Override
            public void changePassword(String oldPassword, String newPassword) {

            }

            @Override
            public boolean userExists(String username) {
                return false;
            }

            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                return null;
            }
        };
    }

}

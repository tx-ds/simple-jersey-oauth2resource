package com.example.oauth;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;

public class MunichAirportRemoteTokenServices extends BaseRemoteTokenServices {

    private TokenStore tokenStore;

    public MunichAirportRemoteTokenServices(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException {
        // simple check that accessToken is within tokenStore
        OAuth2Authentication oAuth2Authentication = tokenStore.readAuthentication(accessToken);

        if(oAuth2Authentication != null) {
            return oAuth2Authentication;
        }
        throw new InvalidTokenException("invalid token");
    }
}

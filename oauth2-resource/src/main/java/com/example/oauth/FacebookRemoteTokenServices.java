package com.example.oauth;

import com.google.common.base.Function;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.social.MissingAuthorizationException;
import org.springframework.social.NotAuthorizedException;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.impl.FacebookTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class FacebookRemoteTokenServices extends BaseRemoteTokenServices {

    private static final Set<String> SCOPES = ImmutableSet.of("facebook", "private", "public", "register");

    private Cache<String, FacebookProfile> cache = CacheBuilder.newBuilder().expireAfterWrite(120, TimeUnit.SECONDS).build();

    public FacebookRemoteTokenServices() {
        super();
        method = HttpMethod.GET;
        tokenName = "access_token";
    }

    /*
     # How to get access_token from facebook manual
     https://developers.facebook.com/tools/explorer/
     */
    @Override
    public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException {
        FacebookProfile profile = cache.getIfPresent(accessToken);
        if(profile == null) {
            Facebook facebook = new FacebookTemplate(accessToken); // use spring social to extract user data from Facebook
            try {
                profile = facebook.userOperations().getUserProfile(); // try to access user data
                cache.put(accessToken, profile);
            } catch (MissingAuthorizationException e) {
                // ok, token valid but have the wrong permission, thats an indicator that you get the wrong typ of access_token
                // see
                // https://developers.facebook.com/docs/facebook-login/access-tokens
                throw new InvalidTokenException("without authorization", e);
            } catch (NotAuthorizedException | UncategorizedApiException e) {
                // invalid token, fine
                throw new InvalidTokenException("invalid token", e);
            }
        }

        Map<String, Object> map = new HashMap<>();
        // TODO munich airport: facebook could return an empty email field if your access_token doesn't enough permission
        map.put(UserAuthenticationConverter.USERNAME, profile.getEmail());
        map.put(AccessTokenConverter.SCOPE, SCOPES);

        // convert scope direct to user authorization roles
        Set<GrantedAuthority> authorities = FluentIterable.from(SCOPES).transform(new Function<String, GrantedAuthority>() {
            @Override
            public GrantedAuthority apply(String input) {
                return new SimpleGrantedAuthority(input);
            }
        }).toImmutableSet();
        map.put(AccessTokenConverter.AUTHORITIES, authorities);
        OAuth2Authentication authentication = tokenConverter.extractAuthentication(map);

        authentication.setDetails(new ObjectMapper().convertValue(profile, Map.class)); // assign everything we know about this Facebook user to the details
        return authentication;
    }
}

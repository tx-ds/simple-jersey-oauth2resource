package com.example.oauth;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DS on 24/11/14.
 */
public class AggregateRemoteTokenServices implements ResourceServerTokenServices {

    private final List<BaseRemoteTokenServices> services = new ArrayList<>();

    @Override
    public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException {
        Exception lastException = null;
        for(BaseRemoteTokenServices service : services) {
            try {
                return service.loadAuthentication(accessToken);
            } catch(AuthenticationException | InvalidTokenException e) {
                lastException = e;
                // ignore, try next
            } catch(HttpClientErrorException e) {
                // ignore
            }
        }
        if(lastException instanceof AuthenticationException) {
            throw (AuthenticationException) lastException;
        } else {
            throw (InvalidTokenException) lastException;
        }
    }

    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        throw new UnsupportedOperationException("Not supported: read access token");
    }

    public void addRemoteTokenService(BaseRemoteTokenServices service) {
        services.add(service);
    }
}

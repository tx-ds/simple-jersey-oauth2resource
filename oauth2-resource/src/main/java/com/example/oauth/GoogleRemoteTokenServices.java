package com.example.oauth;

import com.google.common.base.Function;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by DS on 24/11/14.
 */
public class GoogleRemoteTokenServices extends BaseRemoteTokenServices {

    private static final Set<String> SCOPES = ImmutableSet.of("google", "private", "public", "register");

    private Cache<String, Map<String, Object>> cache = CacheBuilder.newBuilder().expireAfterWrite(120, TimeUnit.SECONDS).build();

    public GoogleRemoteTokenServices() {
        super();
        method = HttpMethod.GET;
        tokenName = "access_token";
    }

    /*
    https://developers.google.com/oauthplayground/
    allow you to access easy an access_token for tests, the scope should be 'email'
     */
    @Override
    public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException {
        Map<String, Object> googleMap = cache.getIfPresent(accessToken);
        if (googleMap == null) {
            MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
            //formData.add(tokenName, accessToken);
            HttpHeaders headers = new HttpHeaders();
            //headers.set("Authorization", getAuthorizationHeader(clientId, clientSecret));

            googleMap = requestForMap(checkTokenEndpointUrl + "?access_token=" + accessToken, formData, headers);

            if (googleMap.containsKey("error")) {
                logger.debug("check_token returned error: " + googleMap.get("error"));
                throw new InvalidTokenException(accessToken);
            }
            cache.put(accessToken, googleMap);
        }
        /* example JSON response
         * {
         "family_name": "Mustermann",
         "name": "Hans Mustermann",
         "picture": "https://lh5.googleusercontent.com/-vHwA5sKmccE/AAAAAAAAAAI/AAAAAAAACpU/IBvYqLXazUk/photo.jpg",
         "gender": "male",
         "email": "hans.mustermann@gmail.com",
         "link": "https://plus.google.com/111557769378905964904",
         "given_name": "Hans",
         "id": "111557769378905964904",
         "verified_email": true
         }
         */

        Map<String, Object> map = new HashMap<>();
        map.put(UserAuthenticationConverter.USERNAME, googleMap.get("email"));
        map.put(AccessTokenConverter.SCOPE, SCOPES);

        // convert scope direct to user authorization roles
        Set<GrantedAuthority> authorities = FluentIterable.from(SCOPES).transform(new Function<String, GrantedAuthority>() {
            @Override
            public GrantedAuthority apply(String input) {
                return new SimpleGrantedAuthority(input);
            }
        }).toImmutableSet();
        map.put(AccessTokenConverter.AUTHORITIES, authorities);

        OAuth2Authentication generate = tokenConverter.extractAuthentication(map);

        generate.setDetails(googleMap); // add every thing we know about this Google user

        return generate;
    }
}

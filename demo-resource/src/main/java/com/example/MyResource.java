package com.example;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.Collection;
import java.util.Set;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

    @Context
    SecurityContext securityContext;

    @Autowired
    UserDetailsManager userDetailsManager;

    @Autowired
    PasswordEncoder passwordEncoder;

//    @Autowired
//    UserDetailsService userDetailsService;


    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        // Demo jersey REST resource
        return "Got it! Hello User " + securityContext.getUserPrincipal().getName();
    }

    @POST
    public void makeUserExample() {
        Set<GrantedAuthority> roles = FluentIterable.from(ImmutableSet.of("role1", "role2")).transform(new Function<String, GrantedAuthority>() {
            @Override
            public GrantedAuthority apply(String input) {
                return new SimpleGrantedAuthority(input);
            }
        }).toImmutableSet();
        User user = new User("username", passwordEncoder.encode("plainpassword"), roles);
        if(userDetailsManager.userExists("username")) {
            userDetailsManager.updateUser(user);
        } else {
            userDetailsManager.createUser(user);
        }
    }
}
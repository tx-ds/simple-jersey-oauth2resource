package com.example.config;

import com.example.oauth.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.provisioning.UserDetailsManager;

@Configuration
public class OAuth2ResourceConfig extends OAuth2Configuration {

    @Configuration
    @EnableResourceServer
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

        @Autowired
        private TokenStore tokenStore;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.resourceId(RESOURCE_ID);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            /*
            Configure which path should be secured and define which authorization condition allow access
             */
            // @formatter:off
            http
                    .requestMatchers().antMatchers("/webapi/**")
                    .and()
                    .authorizeRequests()
                    .antMatchers("/webapi/**").access("#oauth2.hasScope('public')");
            // @formatter:on
        }

        @Bean
        @Primary
        public ResourceServerTokenServices tokenService() {
            // Munich Airport Token Validator
            BaseRemoteTokenServices munichAirportRemoteTokenServices = new MunichAirportRemoteTokenServices(tokenStore);

            // Google Token Validator
            BaseRemoteTokenServices googleRemoteTokenServices = new GoogleRemoteTokenServices();
            // https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=<access_token> to get information about the token
            googleRemoteTokenServices.setCheckTokenEndpointUrl("https://www.googleapis.com/oauth2/v1/userinfo"); // to get information about the user that is connected with this token

            // Facebook Token Validator
            BaseRemoteTokenServices facebookRemoteTokenServices = new FacebookRemoteTokenServices();
            // https://graph.facebook.com/debug_token?input_token=<access_token>&access_token=<access_token> to get information about the token
            facebookRemoteTokenServices.setCheckTokenEndpointUrl("https://graph.facebook.com/me"); // to get information about the user that is connected with this token

            /*
             * 1. Munich Airport (internal call again tokenStore)
             * 2. Google (network call)
             * 3. Facebook (network call)
             * First success will break iterate through loop
             */
            AggregateRemoteTokenServices tokenServices = new AggregateRemoteTokenServices();
            tokenServices.addRemoteTokenService(munichAirportRemoteTokenServices);
            tokenServices.addRemoteTokenService(googleRemoteTokenServices);
            tokenServices.addRemoteTokenService(facebookRemoteTokenServices);
            return tokenServices;
        }

    }

}
